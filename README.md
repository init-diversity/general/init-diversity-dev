## init-diversity-dev
-----------------------

## Summary
---------------------------------

A metapackage with the full collection of development software that is required to build the customised 'init-diversity' packages.
This package can be safely removed after the software it contains is installed.




## Recommended build instructions
---------------------------------

`gbp clone https://gitlab.com/init-diversity/general/init-diversity-dev.git && cd init-diversity-dev && gbp buildpackage`

is the recommended method to build this package directly from git.


`sudo apt install git-buildpackage`

should get you all the software required to build the package using this method.



